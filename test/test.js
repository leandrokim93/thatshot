const assert = require('assert');
const path = require('path');
const api = require(path.resolve(`${__dirname}/../app/api/mvdb`));

global.config = require("../config");

describe('The Movie Database', function () {
    describe('Get Tv Show', function () {
        it('if not specified, should return the first page', function (done) {
            this.timeout(5000);
            api.getTvShows({}, (response) => {
                let { page, total_results, total_pages, results } = response.data;
                assert.equal(page, 1);
                done();
            }, (error) => {
                assert.equal(1, 0)
                done();
            });
        });

        it('if a negative page is entered, it should return page 1', function (done) {
            this.timeout(5000);
            let pageToTest = -1;
            api.getTvShows({page: pageToTest}, (response) => {
                let { page, total_results, total_pages, results } = response.data;
                assert.equal(page, 1);
                done();
            }, (error) => {
                //IF ERROR, DOESNT PASS TEST, AND SHOULD BE FIXED!
                assert.equal(1, pageToTest, error.response.data.errors[0])
                done();
            });
        });
    });
});