const axios = require('axios');

const mod = {};

mod.getTvShows = (params, callback, error) => {
    const { page, sort } = params;
    axios.get(config.baseUrl, {
        params: {
            page: page || 1,
            api_key: config.apiKey,
            sort_by: sort || config.sort.POPULAR_DESC
        }
    })
        .then(callback)
        .catch(error);
};

mod.getTvShowDetail = (params, callback, error) => {
    const { id } = params;
    axios.get(`${config.tvUrl}${id}`, {
        params: {
            api_key: config.apiKey,
        }
    })
        .then(callback)
        .catch(error);
};

mod.getTvShowCast = (params, callback, error) => {
    const { id } = params;
    axios.get(`${config.tvUrl}${id}${config.creditPath}`, {
        params: {
            api_key: config.apiKey,
        }
    })
        .then(callback)
        .catch(error);
};


module.exports = mod;