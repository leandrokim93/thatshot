const express = require("express");
const path = require('path');
const api = require(path.resolve(`${__dirname}/../api/mvdb`));

const router = express.Router();

router.get("/details", (req, res) => {
    res.sendFile(path.resolve(`${__dirname}/../../public/html/details.html`));
});

router.get("/api/list", (req, res) => {
    api.getTvShows(req.query, (response) => {
        res.json(response.data);
    }, (error) => {
        console.error(error.response.data);
        res.status(error.response.status).json();
    });
});

router.get("/api/detail", (req, res) => {
    api.getTvShowDetail(req.query, (response) => {
        res.json(response.data);
    }, (error) => {
        console.error(error.response.data);
        res.status(error.response.status).json();
    });
});

router.get("/api/credit", (req, res) => {
    api.getTvShowCast(req.query, (response) => {
        res.json(response.data);
    }, (error) => {
        console.error(error.response.data);
        res.status(error.response.status).json();
    });
});

module.exports = router;