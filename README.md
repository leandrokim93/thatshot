# That's Hot!

Its and app to see the currentyle trending tv shows.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
npm install
```

### Installing

To run the server:
```
node index.js
```

## Running the tests

To run the test, 

```
npm i -D
```

And then run:

```
npm test
```

## Built With

* [NodeJS](https://nodejs.org/en/) - Server framework
* [VueJS](https://vuejs.org/) - HTML framework
* [Mocha](https://mochajs.org/) - Test environment

## Authors

* **Leandro Kim** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
