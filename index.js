'use strict'

global.config = require("./config");

const express = require("express");
const bodyParser = require("body-parser");

const server = express();
const indexRouter = require(`${__dirname}/app/routers/index`);

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.use(express.static(`${__dirname}/public`));
server.use(indexRouter);

server.listen(config.port, (err)=>{
    if(err)
        throw err;
    else
        console.info(`The server is running on port: ${config.port}`);
});
