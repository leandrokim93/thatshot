var app = new Vue({
    el: '#app',
    data: {
        loading: true,
        details: {},
        credits: {}
    },
    computed: {
        orderedCasts: function () {
            return _.orderBy(this.credits.cast, 'order')
          }
    },
});



refreshDetail(getId(), () => {
    app.loading = false;
});

function refreshDetail(id, callback) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/api/detail",
        "method": "GET",
        "headers": {},
        "data": { id }
    };

    $.ajax(settings).done(function (response) {
        app.details = response;

        if (callback) {
            callback();
        }
    });

    var settings2 = {
        "async": true,
        "crossDomain": true,
        "url": "/api/credit",
        "method": "GET",
        "headers": {},
        "data": { id }
    };

    $.ajax(settings2).done(function (response) {
        app.credits = response;
    });
}

function getId() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
    return id;
}