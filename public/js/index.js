var app = new Vue({
    el: '#app',
    data: {
        loading: true,
        loading2: false,
        list: [],
        pageNumbers: [],
        currentPage: 1
    },
    methods: {
        changePage(index) {
            if (index == this.currentPage) {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
                return;
            }
            this.currentPage = index;
            this.loading2 = true;
            this.list = [];
            refreshList(index, () => {
                this.loading2 = false;
            });
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    },
});

refreshList(1, () => {
    app.loading = false;
});

function refreshList(page, callback) {
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "/api/list",
        "method": "GET",
        "headers": {},
        "data": {}
    };
    
    if (page) {
        settings.data.page = page;
    }

    $.ajax(settings).done(function (response) {
        app.list = response.results;
        app.pageNumbers = indexPages(response.page, response.total_pages);
        if (callback) {
            callback();
        }
    });
}

function indexPages(page, totalPages) {
    if (totalPages >= 1000) {
        totalPages = 1000;
    }
    let pages = [1];
    let start = page - 3 <= 1 ? 2 : page - 3;
    let end = page + 3 >= totalPages - 1 ? totalPages - 2 : page + 3;
    if (start != 2) {
        pages.push(0);
    }
    for (let i = start; i < end; i++) {
        pages.push(i);
    }
    if (end != totalPages) {
        pages.push(0);
    }
    pages.push(totalPages - 1);
    return pages;
}